let r: number = 0.21;
let g: number = 0.72;
let b: number = 0.07;

let maxr: Map<number, number> = new Map<number, number>();

function arrayMin(a: number[], from: number = 0, to: number = a.length - 1) {
  let r = a[from];
  for (let i: number = from + 1; i <= to; i++) {
    if (r > a[i]) {
      r = a[i];
    }
  }
  return r;
}

class Vec {
  constructor(private x: number, private y: number, private z: number) {};
  public plus(v: Vec): Vec {
    return new Vec(this.x + v.x, this.y + v.y, this.z + v.z);
  }
  public minus(v: Vec): Vec {
    return new Vec(this.x - v.x, this.y - v.y, this.z - v.z);
  }
  public mul(c: number): Vec {
    return new Vec(this.x * c, this.y * c, this.z * c);
  }
  public mulScalar(v: Vec): number {
    return this.x * v.x + this.y * v.y + this.z * v.z;
  }
  public length(): number {
    return Math.sqrt(this.lengthSquare());
  }
  public lengthSquare(): number {
    return this.x * this.x + this.y * this.y + this.z * this.z;
  }
  public norm(): Vec {
    let l: number = this.length();
    return new Vec(this.x / l, this.y / l, this.z / l);
  }
  public project(vec: Vec): Vec {
    return vec.mul(this.mulScalar(vec) / this.lengthSquare());
  }
  public affinDist(plane: Plane, dir: Vec): number {
    let p = plane.vec;
    let n = plane.norm;
    let v = this;
    let r = dir;
    let D = (p.mulScalar(n) - v.mulScalar(n)) / (n.lengthSquare() * r.lengthSquare() - r.mulScalar(n) * r.mulScalar(n));
    let b = n.mul(D * r.lengthSquare()).plus(r.mul(D * r.mulScalar(n)));
    return b.length();
  }
}

class Plane {
  constructor(public vec: Vec, public norm: Vec) {};
  public affinDist(vec: Vec, dir: Vec): number {
    return vec.affinDist(this, dir);
  }
}

class Cuboid {
  private planes: Array<Plane> = new Array<Plane>();
  public addPlane(plane: Plane) {
    this.planes.push(plane);
  }
  public minRat(vec: Vec, dir: Vec) : number {
    let a = new Array<number>();
    for (let plane of this.planes) {
      a.push(vec.affinDist(plane, dir));
    }
    return arrayMin(a);
  }
}

class Cube extends Cuboid {
  constructor() {
    super();
    this.addPlane(new Plane(new Vec(0, 0, 0), new Vec(1, 0, 0)));
    this.addPlane(new Plane(new Vec(0, 0, 0), new Vec(0, 1, 0)));
    this.addPlane(new Plane(new Vec(0, 0, 0), new Vec(0, 0, 1)));
    this.addPlane(new Plane(new Vec(1, 1, 1), new Vec(1, 0, 0)));
    this.addPlane(new Plane(new Vec(1, 1, 1), new Vec(0, 1, 0)));
    this.addPlane(new Plane(new Vec(1, 1, 1), new Vec(0, 0, 1)));
  }
}

let rgb: Vec = new Vec(r, g, b);

console.log("x");

function main() {
  console.log("x");
  let cube: Cube = new Cube();
  let r = new Array<number>();
  r[0] = 0;
  for (let a: number = 1; a < 256; a++) {
    r[a] = cube.minRat(new Vec(a / 256, a / 256, a / 256), rgb);
    console.log(a + " " + r[a]);
  }

  let maxVol = 0;
  let maxFrom = 0;
  let maxTo = 0;

  for (let a: number = 0; a < 256; a++) {
    for (let b: number = a + 1; b < 256; b++) {
      let min = arrayMin(r, a, b);
      let vol = min * min * Math.PI * (b - a) * 65536;
      if (vol > maxVol) {
        maxVol = vol;
        maxFrom = a;
        maxTo = b;
        console.log("maxvol: " + vol + " from: " + a + " to: " + b + " r: " + min);
      }
    }
  }
}

main();
